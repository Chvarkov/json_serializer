class Car(object):
    mark = None
    model = None
    year = None
    color = None
    working_condition = None
    options = []
    owners = ()
    notes = None
