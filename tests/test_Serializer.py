import unittest
from tests.classes import Car
from tests.classes import TreeNode
from json_serializer.Serializer import Serializer


class TestSerializer(unittest.TestCase):

    PATH_TO_SIMPLE_OBJECT = 'tests/json/car.json'
    PATH_TO_LIST_OF_SCALAR = 'tests/json/listOfScalar.json'
    PATH_TO_LIST_OF_OBJECTS = 'tests/json/listOfObjects.json'
    PATH_TO_TREE = 'tests/json/tree.json'

    def test_serialize_simple_object(self):
        serializer = Serializer([
            Car
        ])

        car = self.get_simple_object()

        actual_json = serializer.serialize(car)
        self.assertIsInstance(actual_json, str)

        expected_json = TestSerializer.get_json_from_file(self.PATH_TO_SIMPLE_OBJECT)
        self.assertEqual(expected_json, actual_json)

    def test_deserialize_simple_object(self):
        serializer = Serializer([
            Car
        ])

        json_string = self.get_json_from_file(self.PATH_TO_SIMPLE_OBJECT)

        actual_car = serializer.deserialize(json_string)
        expected_car = self.get_simple_object()

        self.assertIsInstance(actual_car, Car)
        self.assertEqual(expected_car.model, actual_car.model)
        self.assertEqual(expected_car.mark, actual_car.mark)
        self.assertEqual(expected_car.year, actual_car.year)
        self.assertEqual(expected_car.color, actual_car.color)
        self.assertEqual(expected_car.working_condition, actual_car.working_condition)
        self.assertEqual(expected_car.options, actual_car.options)
        self.assertEqual(expected_car.notes, actual_car.notes)

        owners = []
        for owner in expected_car.owners:
            owners.append(list(owner))

        self.assertEqual(owners, actual_car.owners)

    def test_serialize_list_of_scalar(self):
        expected_json = self.get_json_from_file(self.PATH_TO_LIST_OF_SCALAR)

        serializer = Serializer([])
        actual_json = serializer.serialize(self.get_list_of_scalar())

        self.assertTrue(expected_json, actual_json)

    def test_deserialize_list_of_scalar(self):
        string_json = self.get_json_from_file(self.PATH_TO_LIST_OF_SCALAR)

        serializer = Serializer([])
        actual_list = serializer.deserialize(string_json)
        expected_list = self.get_list_of_scalar()

        self.assertTrue(expected_list, actual_list)

    def test_serialize_list_of_objects(self):
        expected_json = self.get_json_from_file(self.PATH_TO_LIST_OF_OBJECTS)

        serializer = Serializer([
            Car
        ])
        actual_json = serializer.serialize(self.get_list_of_objects())

        self.assertTrue(expected_json, actual_json)

    def test_deserialize_list_of_objects(self):
        string_json = self.get_json_from_file(self.PATH_TO_LIST_OF_OBJECTS)

        serializer = Serializer([
            Car
        ])
        actual_cars = serializer.deserialize(string_json)
        expected_cars = self.get_list_of_objects()

        for index, actual_car in enumerate(actual_cars):

            self.assertIsInstance(actual_car, Car)
            self.assertEqual(expected_cars[index].model, actual_car.model)
            self.assertEqual(expected_cars[index].mark, actual_car.mark)
            self.assertEqual(expected_cars[index].year, actual_car.year)
            self.assertEqual(expected_cars[index].color, actual_car.color)
            self.assertEqual(expected_cars[index].working_condition, actual_car.working_condition)
            self.assertEqual(expected_cars[index].options, actual_car.options)
            self.assertEqual(expected_cars[index].notes, actual_car.notes)

            owners = []
            for owner in expected_cars[index].owners:
                owners.append(list(owner))

            self.assertEqual(owners, actual_car.owners)

    def test_serialize_tree(self):
        expected_json = self.get_json_from_file(self.PATH_TO_TREE)

        serializer = Serializer([
            TreeNode
        ])

        tree = self.get_tree()

        actual_json = serializer.serialize(tree)

        self.assertTrue(expected_json, actual_json)

    def test_deserialize_tree(self):
        string_json = self.get_json_from_file(self.PATH_TO_TREE)

        serializer = Serializer([
            TreeNode
        ])
        actual_list = serializer.deserialize(string_json)
        expected_list = self.get_list_of_scalar()

        self.assertTrue(expected_list, actual_list)

    def test_unknown_object(self):
        json_string = self.get_json_from_file(self.PATH_TO_SIMPLE_OBJECT)

        serializer = Serializer([])
        print(callable(serializer.deserialize))

        with self.assertRaises(Exception):
            serializer.deserialize(json_string)

    @staticmethod
    def get_tree():
        root = TreeNode()
        root.childs = [TreeNode(), TreeNode(), TreeNode()]
        root.childs[0].childs = [TreeNode(), TreeNode(), TreeNode()]
        root.childs[1].childs = [TreeNode(), TreeNode()]
        root.childs[2].childs = [TreeNode()]

        root.childs[0].childs[0].childs = [TreeNode(), TreeNode(), TreeNode()]
        root.childs[0].childs[1].childs = [TreeNode(), TreeNode(), TreeNode()]
        root.childs[0].childs[2].childs = [TreeNode(), TreeNode(), TreeNode()]

        root.childs[1].childs[0].childs = [TreeNode(), TreeNode(), TreeNode()]
        root.childs[1].childs[1].childs = [TreeNode(), TreeNode(), TreeNode()]

        root.childs[2].childs[0].childs = [TreeNode(), TreeNode(), TreeNode()]

        return root

    @staticmethod
    def get_list_of_objects():
        cars = [Car(), Car(), Car()]

        cars[0].mark = 'Audi'
        cars[0].model = 'A4'
        cars[0].year = 2007
        cars[0].color = 'Silver'
        cars[0].working_condition = True
        cars[0].options = [
            'Heating',
            'APS',
            'Navigator'
        ]
        cars[0].owners = (
            (2007, 'Jack'),
            (2012, 'Sarah'),
            (2016, 'Frank'),
            (2018, 'Jim')
        )

        cars[1].mark = 'BMW'
        cars[1].model = 'M6'
        cars[1].year = 2009
        cars[1].color = 'Black'
        cars[1].working_condition = True
        cars[1].options = [
            'Parking sensors',
            'Heating',
            'APS',
        ]
        cars[1].owners = (
            (2009, 'Jim'),
            (2015, 'Tony'),
        )

        cars[2].mark = 'Ford'
        cars[2].model = 'Mustang'
        cars[2].year = 2014
        cars[2].color = 'Yellow'
        cars[2].working_condition = True
        cars[2].options = [
            'Parking sensors',
            'APS',
            'Navigator'
        ]
        cars[2].owners = (
            (2015, 'Alex'),
        )

        return cars

    @staticmethod
    def get_simple_object():
        car = Car()
        car.mark = 'Audi'
        car.model = 'A4'
        car.year = 2007
        car.color = 'Silver'
        car.working_condition = True

        car.options = [
            'Parking sensors',
            'Heating',
            'APS',
            'Navigator'
        ]

        car.owners = (
            (2007, 'Jack'),
            (2012, 'Sarah'),
            (2016, 'Frank'),
            (2018, 'Jim')
        )

        return car

    @staticmethod
    def get_list_of_scalar():
        return ["string", 120, True, 3.14]

    @staticmethod
    def get_json_from_file(filename):
        json_file = open(filename, 'r')
        json = json_file.read()
        json_file.close()
        return json
